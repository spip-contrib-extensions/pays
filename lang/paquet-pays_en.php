<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-pays
// Langue: en
// Date: 19-11-2011 20:53:01
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// P
	'pays_description' => 'This plugin includes the official country names and codes list provided by the [International Orgnisation for Standardization->https://www.iso.org/fr/iso-3166-country-codes.html]. The country table includes a primary key id_pays used by spip_geographie plugin, an ISO 3166-1 2 code as key, and country names in different languages.',
	'pays_nom' => 'Countries ISO 3166-1',
	'pays_slogan' => 'Official country names and codes list',
);
?>