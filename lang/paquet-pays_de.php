<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-pays
// Langue: fr
// Date: 19-11-2011 20:53:01
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// P
	'pays_description' => 'Dieses Plugin stellt die offizielle Länderliste der [Organisation Internationale de Normalisation->https://www.iso.org/fr/iso-3166-country-codes.html] zur Verfügung. Die Tabell enthält das nummerische Feld id_pays aus dem Plugin spip_geographie, den Läbdercode nach ISO3166-1 und den Namen des Landes in mehreren Sprachen.',
	'pays_nom' => 'Land nach ISO 3166-1',
	'pays_slogan' => 'Offizielle Länderliste',
);
?>